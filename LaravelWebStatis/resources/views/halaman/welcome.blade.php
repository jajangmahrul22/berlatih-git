@extends('layouts.master')
@section('judul')
    Halaman Index
@endsection
@section('container')
<body>
  <h1>SELAMAT DATANG! {{ $firstName }} {{ $lastName }}</h1>
  <p style="font-weight: bold">Terima kasih telah bergabung di Website kami. Media Belajar kita bersama!</p>
</body>
@endsection