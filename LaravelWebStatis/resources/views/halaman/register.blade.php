@extends('layouts.master')
@section('judul')
Halaman Form
@endsection
@section('container')    
<body>
  <h1>Buat Account Baru</h1>
  <h2>Sign Up Form</h2>
  <form  method="POST" action="{{ route('welcome') }}">
    @csrf
    <label for="">First Name :</label>
    <br>
    <br>
    <input type="text" name="firstName" id="">
    <br>
    <br>
    <label for="">Last Name :</label>
    <br>
    <br>
    <input type="text" name="lastName" id="">
    <br>
    <br>
    <label for="">Gender</label>
    <br>
    <br>
    <input type="radio" name="gender" id=""> Male <br>
    <input type="radio" name="gender" id=""> Female <br> <br>

    <label for="">Nationality</label> 
    <br> 
    <br>
    <select name="" id=""> 
      <option value="">Indonesia</option>
      <option value="">Singapura</option>
      <option value="">Jepang</option>
    </select>
    <br>
    <br>
    <label for="">Languange Spoken</label> 
    <br> 
    <br>
    <input type="checkbox" name="" id=""> Bahasa Indonesia <br>
    <input type="checkbox" name="" id=""> English <br>
    <input type="checkbox" name="" id=""> Other <br> <br>

    <label for="">Bio</label>
    <br>
    <br>
    <textarea name="" id="" cols="30" rows="10"></textarea> <br>
    <button type="submit">Sign Up</button>


  </form>
</body>
@endsection
