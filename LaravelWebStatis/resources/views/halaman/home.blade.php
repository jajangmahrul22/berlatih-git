@extends('layouts.master')
@section('judul')
    Halaman Home
@endsection

@section('container')    
<body>
  <h1>Media Online</h1>
  <h2>Sosial Media Developer</h2>
  <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>
  <h2>Benefit Join di Media Online</h2>
  <ul>
    <li>Mendapatkan motivasi dari sesama para Developer</li>
    <li>Sharing knowladge</li>
    <li>Dibuat oleh calon web developer terbaik</li>
  </ul>
  <h2>Cara Bergabung ke Media Online</h2>
  <ol>
    <li>Mengunjugi Website ini</li>
    <li>Mendaftarkan di <a href="{{ route('register') }}">Form Sign Up</a></li>
    <li>Selesai</li>
  </ol>
</body>
@endsection
