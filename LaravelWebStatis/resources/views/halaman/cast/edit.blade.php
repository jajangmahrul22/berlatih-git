@extends('layouts.master')
@section('judul')
    Edit Cast
@endsection

@section('container')
<div class="col-lg-8">
  <form method="post" action="/cast/{{ $cast->id }}" class="mb-5">
    @method('PUT')
    @csrf
    <div class="mb-3">
      <label for="nama" class="form-label ">Nama</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama', $cast->nama) }}" required autofocus>
      @error('nama')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="umur" class="form-label ">Umur</label>
      <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" value="{{ old('umur', $cast->umur) }}" required placeholder="Umur">
      @error('umur')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="bio" class="form-label">Bio</label>
      <textarea class="form-control" name="bio" id="bio" rows="3" placeholder="Bio">{{ old('bio', $cast->bio) }}</textarea>
      @error('bio')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>

    <button type="submit" class="btn btn-primary">Edit Cast</button>
  </form>
</div>
@endsection