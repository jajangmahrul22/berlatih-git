@extends('layouts.master')
@section('judul')
    Cast Page
@endsection

@section('container')
@if (session()->has('success'))
<div class="alert alert-success alert-dismissible fade show" role="alert">
  <strong>{{ session('success') }}</strong>
  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
    <span aria-hidden="true">&times;</span>
  </button>
</div>
@endif

<div class="container">
  @auth
  <a href="/cast/create" class="btn btn-primary">Create new cast</a>
  @endauth
  <hr>
  <div class="row">
    @foreach ($casts as $cast)
    <div class="col-md-4 mb-3">
      <div class="card">
        <a href="/cast/{{ $cast->id }}">
          <img src="https://source.unsplash.com/OI1ToozsKBw/500x400" class="img-fluid" alt="{{ $cast->nama }}">
          <div class="card-body">
            <div class="d-flex justify-content-between align-items-start">
              <h3 class="card-title font-weight-bold" style="margin-right: auto"><a href="#" class="text-decoration-none text-dark">{{ $cast->nama }}</a></h3>
              <p class="text-muted">
                  Age {{ $cast->umur }}
              </p>
            </div>
            <p class="card-text">{{ $cast->bio }}</p>
            <a href="/cast/{{ $cast->id }}" class="btn btn-primary">Read more</a>
          </div>
        </a>
      </div>
    </div>    
    @endforeach
  </div>
</div>
@endsection