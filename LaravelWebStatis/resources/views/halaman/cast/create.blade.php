@extends('layouts.master')
@section('judul')
    Create New Cast
@endsection

@section('container')
<div class="col-lg-8">
  <form method="post" action="/cast" class="mb-5">
    @csrf
    <div class="mb-3">
      <label for="nama" class="form-label ">Nama</label>
      <input type="text" class="form-control @error('nama') is-invalid @enderror" id="nama" name="nama" value="{{ old('nama') }}" required autofocus>
      @error('nama')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="umur" class="form-label ">Umur</label>
      <input type="text" class="form-control @error('umur') is-invalid @enderror" id="umur" name="umur" value="{{ old('umur') }}" required  placeholder="Umur">
      @error('umur')
      <div class="invalid-feedback">
        {{ $message }}
      </div>
      @enderror
    </div>
    <div class="mb-3">
      <label for="bio" class="form-label">Bio</label>
      <textarea class="form-control" name="bio" id="bio" rows="3" placeholder="Bio" >{{ old('bio') }}</textarea>
      @error('bio')
      <small><p class="text-danger">{{ $message }}</p></small>
      @enderror
    </div>

    <button type="submit" class="btn btn-primary">Create Cast</button>
  </form>
</div>
@endsection