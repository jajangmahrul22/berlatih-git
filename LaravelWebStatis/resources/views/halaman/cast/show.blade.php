@extends('layouts.master')
@section('judul')
    Cast Detail
@endsection

@section('container')
<div class="container">
  <div class="row my-3">
    <div class="col-lg-8">
      <a href="/cast" class="btn btn-success text-white"><i class="fas fa-arrow-left" style="color: #ffffff;"></i> Back to all cast</a>
      @auth
      <a href="/cast/{{ $cast->id }}/edit" class="btn btn-warning text-white"><i class="fas fa-edit"></i> Edit</a>
      <form class="d-inline" action="/cast/{{ $cast->id }}" method="post">
        @method('delete')  
        @csrf
        <button type="submit" class="btn btn-danger border-0" onclick="return confirm('Are you sure?')">
          <i class="fas fa-trash-alt"></i> Delete</button>
      </form>
      @endauth
      <img src="https://source.unsplash.com/OI1ToozsKBw/1200x400" class="img-fluid mt-3 mb-3" alt="{{ $cast->nama }}">
      <h2>{{ $cast->nama }}</h2>
      <p>Age. {{ $cast->umur }}</p>
      <article class="my-3 fs-5">
       {{ $cast->bio }}
      </article>
    </div>
  </div>
</div>
@endsection