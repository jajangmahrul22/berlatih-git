<?php
require_once("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");
echo "Name: ". $sheep->getName() . PHP_EOL;
echo "Legs: ". $sheep->getLegs() . PHP_EOL;
echo "Cold Blooded: ". $sheep->getColdBlooded() . PHP_EOL;

$kodok = new Frog("buduk");
echo "Name: " . $kodok->getName() .PHP_EOL; 
echo "Legs: " . $kodok->getLegs() .PHP_EOL;
echo "Cold Blooded: " . $kodok->getColdBlooded() .PHP_EOL;
echo "Jump: ";
$kodok->jump();

echo PHP_EOL;

$sungokong = new Ape("kera sakti");
echo "Name: " . $sungokong->getName() .PHP_EOL; 
echo "Legs: " . $sungokong->getLegs() .PHP_EOL;
echo "Cold Blooded: " . $sungokong->getColdBlooded() .PHP_EOL;
echo "Yell: ";
$sungokong->yell();